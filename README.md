<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/sass/sass-original.svg" width="40" height="40"/>
          
### Scss/React Mobile First Webpage with light/dark mode - using flexbox & grid

<a href="https://react-scss-mobile-first-ehya.netlify.app/">Netlify link</a>

<img src="readme-img.png" alt="lifecycle methods" width="700" />
